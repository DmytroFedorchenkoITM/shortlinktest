﻿using AutoMapper;
using ShortLinkTest.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShortLinkTest.Repository.DTO.Map
{
    public class LinkMapping : Profile
    {
        public LinkMapping()
        {
            CreateMap<Link, LinkModel>();
        }
    }
}
