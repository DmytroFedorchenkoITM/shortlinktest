﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShortLinkTest.Repository.DTO.Validators
{
    public class LinkValidator : AbstractValidator<LinkModel>
    {
        public LinkValidator()
        {
            RuleFor(l => l.Name).NotEmpty();

            RuleFor(l => l.FullLink)
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(256);

            RuleFor(l => l.ShortLink)
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(20);
        }
    }
}
