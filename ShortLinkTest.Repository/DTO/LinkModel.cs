﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ShortLinkTest.Repository.DTO
{
    public class LinkModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [DisplayName("Full URL")]
        public string FullLink { get; set; }

        [DisplayName("Short URL")]
        public string ShortLink { get; set; }
    }
}
