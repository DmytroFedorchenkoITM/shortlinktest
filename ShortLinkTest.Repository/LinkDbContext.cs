﻿using Microsoft.EntityFrameworkCore;
using ShortLinkTest.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShortLinkTest.Repository
{
    public class LinkDbContext : DbContext
    {
        public LinkDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Link> Links { get; set; }
    }
}
