﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShortLinkTest.Repository.Entities
{
    public class Link
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [StringLength(256, MinimumLength = 3)]
        public string FullLink { get; set; }

        [Required]
        
        [StringLength(20, MinimumLength = 3)]
        public string ShortLink { get; set; }
    }
}
