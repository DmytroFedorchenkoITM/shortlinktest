﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShortLinkTest.Repository.DTO;
using ShortLinkTest.Repository.DTO.Validators;
using ShortLinkTest.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortLinkTest.Repository.Services
{
    public class LinkService : ILinkService
    {
        private readonly IMapper mapper;
        private readonly LinkDbContext dbContext;

        public LinkService(IMapper mapper, LinkDbContext dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
        }

        public async Task<IList<LinkModel>> GetAll()
        {
            var entities = await dbContext.Links
                .ToListAsync();

            return mapper.Map<List<Link>, List<LinkModel>>(entities);
        }

        public async Task<LinkModel> Get(int id) => mapper.Map<LinkModel>(await dbContext.Links.FindAsync(id));

        public async Task<LinkModel> GetByShortLink(string shortLink)
        {
            var entity = await dbContext.Links
                .SingleOrDefaultAsync(e => e.ShortLink == shortLink);

            return mapper.Map<LinkModel>(entity);
        }

        public async Task Save(LinkModel model)
        {
            var validator = new LinkValidator();
            if (!validator.Validate(model).IsValid)
                throw new ValidationException("Validation error");

            var entity = await dbContext.Links.FindAsync(model.Id);

            if (entity == null)
            {
                // Unique short url checking.
                var byShort = await dbContext.Links
                    .SingleOrDefaultAsync(e => e.ShortLink == model.ShortLink);
                if (byShort != null)
                    throw new ValidationException("Not unique short Url");

                entity = new Link();
                await dbContext.Links.AddAsync(entity);
            }

            entity.Name = model.Name;
            entity.FullLink = model.FullLink;
            entity.ShortLink = model.ShortLink;

            await dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var entity = await dbContext.Links.FindAsync(id);
            dbContext.Links.Remove(entity);
            await dbContext.SaveChangesAsync();
        }
    }
}
