﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ShortLinkTest.Repository.DTO;

namespace ShortLinkTest.Repository.Services
{
    public interface ILinkService
    {
        Task<LinkModel> Get(int id);
        Task<IList<LinkModel>> GetAll();
        Task<LinkModel> GetByShortLink(string shortLink);
        Task Save(LinkModel model);
        Task Delete(int id);
    }
}