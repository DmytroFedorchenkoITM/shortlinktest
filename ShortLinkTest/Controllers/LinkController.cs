﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShortLinkTest.Repository.DTO;
using ShortLinkTest.Repository.Services;

namespace ShortLinkTest.Controllers
{
    public class LinkController : Controller
    {
        private readonly ILinkService linkService;

        public LinkController(ILinkService linkService)
        {
            this.linkService = linkService;
        }

        [HttpGet]
        [Route("{shortLink}")]
        public async Task<IActionResult> Index(string shortLink)
        {
            var link = await linkService.GetByShortLink(shortLink);
            if (link != null)
                return Redirect(link.FullLink);
            else
                return RedirectToAction("Grid");
        }

        [HttpGet]
        public async Task<IActionResult> Grid()
        {
            var links = await linkService.GetAll();
            return View(links);
        }

        [HttpGet]
        [Route("Edit")]
        [Route("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var link = await linkService.Get(id);
            return View(link ?? new LinkModel());
        }

        [HttpPost]
        public async Task<IActionResult> Save(LinkModel model)
        {
            await linkService.Save(model);
            return RedirectToAction("Grid");
        }

        public async Task<IActionResult> Delete(int id)
        {
            await linkService.Delete(id);
            return RedirectToAction("Grid");
        }
    }
}